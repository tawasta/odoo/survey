[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
[![Pipeline Status](https://gitlab.com/tawasta/odoo/survey/badges/14.0-dev/pipeline.svg)](https://gitlab.com/tawasta/odoo/survey/-/pipelines/)

Survey
======
Addons expanding the survey-module

[//]: # (addons)

Available addons
----------------
addon | version | maintainers | summary
--- | --- | --- | ---
[survey_question_attachment](survey_question_attachment/) | 17.0.1.0.0 |  | Adds a new question type 'attachment' to survey
[survey_question_privacy](survey_question_privacy/) | 17.0.1.0.0 |  | Ability to add privacies as questions to survey
[survey_question_ssn](survey_question_ssn/) | 17.0.1.0.0 |  | Ability to add ssn as questions to survey
[survey_sequence](survey_sequence/) | 17.0.1.0.0 |  | Order Surveys by sequence
[survey_string_answer](survey_string_answer/) | 17.0.1.0 |  | Save each answer value as string
[survey_user_input_report_xlsx](survey_user_input_report_xlsx/) | 17.0.1.1.0 |  | Print an xlsx report from survey user inputs

[//]: # (end addons)
